<?php require_once('include/header.php');
      require("tools/default.php");
 ?>
<body>
  <header class="w3-row">
    <ul class="w3-navbar w3-card-2 w3-padding-left ">
      <li class="w3-padding w3-col l1 m1 s5 "><img src="img/logo/logo.png" alt="Nguimbi Logo" class="w3-image" style="width:80%  "/> </li>
      <li class="w3-col l3 m3 s3"><a href="#"> Acerca de n&oacute;s</a></li>
      <li class="w3-dropdown-hover w3-col l2 w3-right ">
        <a href="#">L&iacute;ngua <i class="fa fa-caret-down"></i></a>
        <div class="w3-dropdown-content w3-card-2">
          <a href="#">Ingl&ecirc;s</a>
          <a href="#">Franc&ecirc;s</a>
          <a href="#">Espanhol</a>
          <a href="#">Alem&atilde;o</a>
          <a href="#">Russo</a>
        </div>
      </li>
      <li></li>
      <li></li>
    </ul>
  </header><!-- End of the header -->
 <div class="w3-row w3-container ">

   <h1>
     Quais sao os teus interesses?
   </h1>
   <form class="" action="index.php" method="post">
     <input list="userLocation" name="name" value="" class="w3-round w3-input" placeholder="Escolha a tua localiza&ccedil;&atilde;o">
     <datalist id="userLocation">
       <option value="Luanda">
       <option value="Bi&eacute;">
        <option value="Bengo">
        <option value="Benguela">
        <option value="Cabinda">
        <option value="Cunene">
        <option value="Cuanza Sul">
        <option value="Cuanza Norte">
        <option value="Cuando Cubango">
        <option value="Huambo">
        <option value="H&uacute;ila">
        <option value="Lunda Norte">
        <option value="Lunda Sul">
        <option value="Malange">
        <option value="Moxico">
        <option value="Namibe">
        <option value="Uige">
        <option value="Zaire">
     </datalist>
   </form>
   <p class="w3-btn w3-blue-grey w3-margin-left w3-padding-left w3-round-small ">Selecionar tudo</p>
   <p class="w3-right w3-btn w3-round w3-blue-grey">Pr&oacute;ximo</p>
   <p class="w3-clear"> </p>
  <div class="w3-row w3-container ">
    <div class="w3-col l3">
      <?php

         $result = listarsimples("TblInterest", "");
         while ($records = mysql_fetch_array($result)) {
         //print_r($records)
       ?>
      <p>
        <input type="checkbox" name="<?php echo $records['InterestDescription']; ?>" value="<?php echo $records['InterestDescription']; ?>" class="w3-check ">
        <label for="" class="w3-padding w3-large" value=""><?php echo $records['InterestDescription']; ?></label>
      </p>

      <?php } ?>
    </div><!-- End of interests section -->
    <div class="w3-col l3 ">
      <?php

         $result = listarsimples("TblInterest", "");
         while ($records = mysql_fetch_array($result)) {
         //print_r($records)
       ?>
      <p>
        <input type="checkbox" name="<?php echo $records['InterestDescription']; ?>" value="<?php echo $records['InterestDescription']; ?>" class="w3-check ">
        <label for="" class="w3-padding w3-large" value=""><?php echo $records['InterestDescription']; ?></label>
      </p>

      <?php } ?>
    </div><!-- End of interests section -->
    <div class="w3-col l4 ">
      <?php

         $result = listarsimples("TblInterest", "");
         while ($records = mysql_fetch_array($result)) {
         //print_r($records)
       ?>
      <p>
        <input type="checkbox" name="<?php echo $records['InterestDescription']; ?>" value="<?php echo $records['InterestDescription']; ?>" class="w3-check ">
        <label for="" class="w3-padding w3-large" value=""><?php echo $records['InterestDescription']; ?></label>
      </p>

      <?php } ?>
    </div><!-- End of interests section -->
  </div>
 </div>
  <?php require_once('include/footer.php') ?>
</body>
</html>
