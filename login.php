<?php require_once('include/header.php') ?>
<body style="">
  <div class="w3-row ">
    <header class="w3-row">
      <ul class="w3-navbar w3-card-2 w3-padding-left ">
        <li class="w3-padding w3-col l1 m1 s5 "><img src="img/logo/logo.png" alt="Nguimbi Logo" class="w3-image" style="width:80%  "/> </li>
        <li class="teste w3-col l3 m3 s3"><a href="#"> Acerca de n&oacute;s</a></li>
        <li class="w3-dropdown-hover w3-col l2 w3-right ">
          <a href="#">L&iacute;ngua <i class="fa fa-caret-down"></i></a>
          <div class="w3-dropdown-content w3-card-2">
            <a href="#">Ingl&ecirc;s</a>
            <a href="#">Franc&ecirc;s</a>
            <a href="#">Espanhol</a>
            <a href="#">Alem&atilde;o</a>
            <a href="#">Russo</a>
          </div>
        </li>
        <li></li>
        <li></li>
      </ul>
    </header><!-- End of the header -->
    <div class="loginpage w3-row w3-container">
      <div class="w3-left w3-col l7 s7 m7 w3-container w3-padding w3-center w3-hide-small">
        <!-- <img src="img/bigphone.png" alt="Big Phone Picture" class="w3-image" style="width:30%"/> -->
      </div><!-- End of left section -->

      <div class=" w3-col l3 s12 m4 w3-padding-16 w3-container">
          <div class="w3-row w3-col l12 w3-border w3-round w3-padding w3-card-2">
            <form class="formLoginUser" action="" method="post">
                  <p>
                    <input type="text" name="UserName" value="" placeholder="email ou nome do utilizador"
                    class="userName userPhone userEmail  w3-input w3-border " required>
                  </p>
                  <p>
                      <input type="password" name="UserPassword" value="" placeholder="Senha" id="userPassword"
                          class="w3-input w3-border " required>
                  </p>
                 <p>
                     <a href="#">Esqueceu a senha?</a>
                      <input type="button" value="Entrar com Telemovel" class="digitsbutton w3-btn w3-right w3-round w3-blue">
                     <input type="button" name="login" value="Entrar" id="loginBtn"
                     class="w3-btn w3-round w3-right w3-blue-grey" required>
                 </p>
            </form>
          </div><!-- End of form wrapper --><br><p class="w3-clear">

          <div class="w3-margin-top">
            <div class="w3-col l12 w3-row w3-col w3-border w3-round w3-padding w3-card-2">
               <form class="formRegisterUser" action="" method="post" name="formRegisterUser">
                <div class=" w3-border-grey w3-round">
                  <span class="w3-meddium">Ainda n&atilde;o est&aacute;s na Nguimbi? Regista-te j&aacute; </span>
                  <p>
                      <input type="text" name="UserName" value="" placeholder="Nome completo" id="userName"
                      class="w3-input w3-border ">
                  </p>
                   <p>
                       <input type="text" placeholder="email" name="UserData" value=""
                        class="w3-input w3-border ">
                   </p>
                   <p>
                       <input type="password" name="UserPassword" value="" placeholder="Senha" id="userPassword"
                        class="w3-input w3-border ">
                   </p>
                   <p>
                       <label for="UserType">Individual</label><input type="radio" name="UserType" value="Individual"  id="UserType"
                        class="w3-border ">
                        <label for="UserType">Empresarial</label><input type="radio" name="UserType" value="Empresarial"  id="UserType"
                         class=" ">
                   </p>
                   <p>
                       <input type="submit" name="" value="Registar"  class="w3-btn w3-right w3-round w3-blue-grey">
                   </p>
                </div><!-- End of Sign-Up section -->
                </form>
              </div>
          </div>
          <ul class="cb-slideshow">
              <li><span>Image 01</span><div><h3></h3></div></li>
              <li><span>Image 02</span><div><h3></h3></div></li>
              <li><span>Image 03</span><div><h3></h3></div></li>
              <li><span>Image 04</span><div><h3></h3></div></li>
              <li><span>Image 05</span><div><h3></h3></div></li>
              <li><span>Image 06</span><div><h3></h3></div></li>
          </ul>
    </div>
    </div>
    <p class="w3-clear">    </p>
    <?php require_once('include/footer.php') ?></div><!--End of main Container -->
</body>
</html>
