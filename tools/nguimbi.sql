-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 27-Mar-2016 às 19:40
-- Versão do servidor: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nguimbi`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `routine1` ()  BEGIN

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TblAddress`
--

CREATE TABLE `TblAddress` (
  `AddressId` int(11) NOT NULL,
  `Address` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TblCategory`
--

CREATE TABLE `TblCategory` (
  `CategoryId` int(11) NOT NULL,
  `CategoryName` varchar(45) NOT NULL,
  `CategoryStatus` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TblCheck-in`
--

CREATE TABLE `TblCheck-in` (
  `ChickId` int(11) NOT NULL,
  `CheckLocal` varchar(20) NOT NULL,
  `CheckDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UserId` int(11) NOT NULL,
  `CheckStatu` tinyint(4) NOT NULL DEFAULT '1',
  `CheckVisibility` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TblComentary`
--

CREATE TABLE `TblComentary` (
  `ComentId` int(11) NOT NULL,
  `ComentDescription` varchar(35) NOT NULL,
  `ComentImage` varchar(20) NOT NULL,
  `ComentDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ComentEstatu` tinyint(4) NOT NULL,
  `PostId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TblCompany`
--

CREATE TABLE `TblCompany` (
  `CompanyId` int(11) NOT NULL,
  `CompanyName` varchar(45) NOT NULL,
  `CompanyEmail` varchar(35) DEFAULT NULL,
  `CompanyPhone` varchar(15) DEFAULT NULL,
  `CompanyDescription` varchar(55) DEFAULT NULL,
  `CompanyLogotype` varchar(20) DEFAULT NULL,
  `CompanyAddress` varchar(45) DEFAULT NULL,
  `CompanySlogan` varchar(30) DEFAULT NULL,
  `CompanyStatu` tinyint(4) DEFAULT NULL,
  `CategoryId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TblEvent`
--

CREATE TABLE `TblEvent` (
  `EventId` int(11) NOT NULL,
  `EventTheme` varchar(25) NOT NULL,
  `EventImage` varchar(20) DEFAULT NULL,
  `EvantDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `EventDescription` varchar(45) DEFAULT NULL,
  `EventLocal` varchar(30) NOT NULL,
  `EventVideo` varchar(25) DEFAULT NULL,
  `EventTitle` varchar(35) NOT NULL,
  `EventVisibility` varchar(15) NOT NULL,
  `EventStatu` tinyint(4) NOT NULL DEFAULT '1',
  `UserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TblFriend`
--

CREATE TABLE `TblFriend` (
  `FriendId` int(11) NOT NULL,
  `FriandEstatu` tinyint(4) NOT NULL DEFAULT '1',
  `UserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TblGroup`
--

CREATE TABLE `TblGroup` (
  `GroupId` int(11) NOT NULL,
  `GroupTitle` varchar(45) NOT NULL,
  `GroupLogotype` varchar(20) NOT NULL,
  `GroupImagecover` varchar(25) NOT NULL,
  `GroupVibility` varchar(15) NOT NULL,
  `GroupStatu` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TblImage`
--

CREATE TABLE `TblImage` (
  `ImageId` int(11) NOT NULL,
  `ImageDescription` varchar(25) NOT NULL,
  `PostId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TblInterest`
--

CREATE TABLE `TblInterest` (
  `InterestId` int(11) NOT NULL,
  `InterestDescription` varchar(25) NOT NULL,
  `InterestStatu` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `TblInterest`
--

INSERT INTO `TblInterest` (`InterestId`, `InterestDescription`, `InterestStatu`) VALUES
(1, 'Cultura', 1),
(2, 'Desporto', 1),
(3, 'Músico', 1),
(4, 'Tecnologia', 1),
(5, 'Entretenimento', 1),
(6, 'Empreendedorismo', 1),
(7, 'Design', 1),
(8, 'Moda', 1),
(9, 'Grupos de Estudo', 1),
(10, 'Investigação Acadêmica', 1),
(11, 'Política', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `TblInterester`
--

CREATE TABLE `TblInterester` (
  `InterestId` int(11) NOT NULL,
  `InterestDescription` varchar(25) NOT NULL,
  `InterestStatu` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `TblInterester`
--

INSERT INTO `TblInterester` (`InterestId`, `InterestDescription`, `InterestStatu`) VALUES
(1, 'Desporto', 0),
(2, 'Desporto', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `TblLike`
--

CREATE TABLE `TblLike` (
  `LikeId` int(11) NOT NULL,
  `PostId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TblMessage`
--

CREATE TABLE `TblMessage` (
  `MessageId` int(11) NOT NULL,
  `MessageText` varchar(45) NOT NULL,
  `MessageData` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UserId` int(11) NOT NULL,
  `MessageStatu` tinyint(4) NOT NULL,
  `FriendId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TblMycompany`
--

CREATE TABLE `TblMycompany` (
  `UserId` int(11) NOT NULL,
  `InteresterId` int(11) NOT NULL,
  `CompanyId` int(11) NOT NULL,
  `MycompanyDatestart` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TblMyinterest`
--

CREATE TABLE `TblMyinterest` (
  `UserId` int(11) NOT NULL,
  `InterestId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TblPeople`
--

CREATE TABLE `TblPeople` (
  `PeopleId` int(11) NOT NULL,
  `PeopleEducation` varchar(35) DEFAULT NULL,
  `PeopleInstitution` varchar(55) DEFAULT NULL,
  `PeopleLocalInstitution` varchar(30) DEFAULT NULL,
  `PeopleBirthDay` date DEFAULT NULL,
  `PeopleNaturalness` varchar(30) DEFAULT NULL,
  `PeopleNationality` varchar(25) DEFAULT NULL,
  `PeopleMaritalstatus` varchar(12) DEFAULT NULL,
  `PeopleGender` varchar(12) DEFAULT NULL,
  `UserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TblPeoplegroup`
--

CREATE TABLE `TblPeoplegroup` (
  `PeopleId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `GroupId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TblPeople_has_TblCompany`
--

CREATE TABLE `TblPeople_has_TblCompany` (
  `TblPeople_PeopleId` int(11) NOT NULL,
  `TblPeople_UserId` int(11) NOT NULL,
  `TblCompany_CompanyId` int(11) NOT NULL,
  `TblCompany_UserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TblPost`
--

CREATE TABLE `TblPost` (
  `PostId` int(11) NOT NULL,
  `PostDescription` varchar(75) NOT NULL,
  `PostImage` varchar(20) NOT NULL,
  `PostVideo` varchar(25) NOT NULL,
  `PostAudeo` varchar(25) NOT NULL,
  `PostDate` timestamp NULL DEFAULT NULL,
  `PostVisibility` varchar(15) NOT NULL,
  `PostStatu` tinyint(4) NOT NULL,
  `UserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TblSession`
--

CREATE TABLE `TblSession` (
  `SessionId` int(11) NOT NULL,
  `SessionStart` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TblSubCategory`
--

CREATE TABLE `TblSubCategory` (
  `SubCategoryId` int(11) NOT NULL,
  `SubCategoryName` varchar(45) NOT NULL,
  `CategoryStatus` tinyint(4) NOT NULL DEFAULT '1',
  `CategoryId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TblUser`
--

CREATE TABLE `TblUser` (
  `UserId` int(11) NOT NULL,
  `UserName` varchar(45) NOT NULL,
  `UserEmail` varchar(25) NOT NULL,
  `UserPhone` varchar(15) NOT NULL,
  `UserPassword` varchar(15) NOT NULL,
  `UserType` varchar(15) NOT NULL,
  `UserAddress` varchar(45) NOT NULL,
  `UserImageprofile` varchar(20) DEFAULT NULL,
  `UserImagecover` varchar(25) DEFAULT NULL,
  `UserStatu` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `TblAddress`
--
ALTER TABLE `TblAddress`
  ADD PRIMARY KEY (`AddressId`);

--
-- Indexes for table `TblCategory`
--
ALTER TABLE `TblCategory`
  ADD PRIMARY KEY (`CategoryId`);

--
-- Indexes for table `TblCheck-in`
--
ALTER TABLE `TblCheck-in`
  ADD PRIMARY KEY (`ChickId`,`UserId`),
  ADD KEY `fk_tbl_check-in_tbl_user1_idx` (`UserId`);

--
-- Indexes for table `TblComentary`
--
ALTER TABLE `TblComentary`
  ADD PRIMARY KEY (`ComentId`,`PostId`,`UserId`),
  ADD KEY `fk_TblComentario_TblPost1_idx` (`PostId`,`UserId`);

--
-- Indexes for table `TblCompany`
--
ALTER TABLE `TblCompany`
  ADD PRIMARY KEY (`CompanyId`,`CategoryId`),
  ADD UNIQUE KEY `CompanyEmail_UNIQUE` (`CompanyEmail`),
  ADD UNIQUE KEY `CompanyPhone_UNIQUE` (`CompanyPhone`),
  ADD KEY `fk_TblCompany_TblCategory1_idx` (`CategoryId`);

--
-- Indexes for table `TblEvent`
--
ALTER TABLE `TblEvent`
  ADD PRIMARY KEY (`EventId`,`UserId`),
  ADD KEY `fk_tbl_eventos_tbl_user1_idx` (`UserId`);

--
-- Indexes for table `TblFriend`
--
ALTER TABLE `TblFriend`
  ADD PRIMARY KEY (`FriendId`,`UserId`),
  ADD KEY `fk_TblFriend_TblUser1_idx` (`UserId`);

--
-- Indexes for table `TblGroup`
--
ALTER TABLE `TblGroup`
  ADD PRIMARY KEY (`GroupId`);

--
-- Indexes for table `TblImage`
--
ALTER TABLE `TblImage`
  ADD PRIMARY KEY (`ImageId`,`PostId`),
  ADD KEY `fk_tbl_imagens_tbl_post1_idx` (`PostId`);

--
-- Indexes for table `TblInterest`
--
ALTER TABLE `TblInterest`
  ADD PRIMARY KEY (`InterestId`);

--
-- Indexes for table `TblInterester`
--
ALTER TABLE `TblInterester`
  ADD PRIMARY KEY (`InterestId`);

--
-- Indexes for table `TblLike`
--
ALTER TABLE `TblLike`
  ADD PRIMARY KEY (`LikeId`,`PostId`,`UserId`),
  ADD KEY `fk_TblGosto_TblPost1_idx` (`PostId`,`UserId`);

--
-- Indexes for table `TblMessage`
--
ALTER TABLE `TblMessage`
  ADD PRIMARY KEY (`MessageId`,`UserId`,`FriendId`),
  ADD KEY `fk_tbl_mensagess_tbl_user1_idx` (`UserId`),
  ADD KEY `fk_TblMessage_TblFriend1_idx` (`FriendId`);

--
-- Indexes for table `TblMycompany`
--
ALTER TABLE `TblMycompany`
  ADD PRIMARY KEY (`UserId`,`InteresterId`,`CompanyId`),
  ADD KEY `fk_TblUser_has_TblCompany_TblCompany1_idx` (`CompanyId`),
  ADD KEY `fk_TblUser_has_TblCompany_TblUser1_idx` (`UserId`,`InteresterId`);

--
-- Indexes for table `TblMyinterest`
--
ALTER TABLE `TblMyinterest`
  ADD PRIMARY KEY (`UserId`,`InterestId`),
  ADD KEY `fk_TblUser_has_TblInterest_TblInterest1_idx` (`InterestId`),
  ADD KEY `fk_TblUser_has_TblInterest_TblUser1_idx` (`UserId`);

--
-- Indexes for table `TblPeople`
--
ALTER TABLE `TblPeople`
  ADD PRIMARY KEY (`PeopleId`,`UserId`),
  ADD KEY `fk_tbl_pessoa_tbl_user1_idx` (`UserId`);

--
-- Indexes for table `TblPeoplegroup`
--
ALTER TABLE `TblPeoplegroup`
  ADD PRIMARY KEY (`PeopleId`,`UserId`,`GroupId`),
  ADD KEY `fk_TblPeople_has_TblGroup_TblGroup1_idx` (`GroupId`),
  ADD KEY `fk_TblPeople_has_TblGroup_TblPeople1_idx` (`PeopleId`,`UserId`);

--
-- Indexes for table `TblPeople_has_TblCompany`
--
ALTER TABLE `TblPeople_has_TblCompany`
  ADD PRIMARY KEY (`TblPeople_PeopleId`,`TblPeople_UserId`,`TblCompany_CompanyId`,`TblCompany_UserId`),
  ADD KEY `fk_TblPeople_has_TblCompany_TblCompany1_idx` (`TblCompany_CompanyId`,`TblCompany_UserId`),
  ADD KEY `fk_TblPeople_has_TblCompany_TblPeople1_idx` (`TblPeople_PeopleId`,`TblPeople_UserId`);

--
-- Indexes for table `TblPost`
--
ALTER TABLE `TblPost`
  ADD PRIMARY KEY (`PostId`,`UserId`),
  ADD KEY `fk_tbl_post_tbl_user1_idx` (`UserId`);

--
-- Indexes for table `TblSession`
--
ALTER TABLE `TblSession`
  ADD PRIMARY KEY (`SessionId`,`UserId`),
  ADD KEY `fk_tbl_sessao_tbl_user1_idx` (`UserId`);

--
-- Indexes for table `TblSubCategory`
--
ALTER TABLE `TblSubCategory`
  ADD PRIMARY KEY (`SubCategoryId`,`CategoryId`),
  ADD KEY `fk_TblSubCategory_TblCategory1_idx` (`CategoryId`);

--
-- Indexes for table `TblUser`
--
ALTER TABLE `TblUser`
  ADD PRIMARY KEY (`UserId`),
  ADD UNIQUE KEY `email_user_UNIQUE` (`UserEmail`),
  ADD UNIQUE KEY `UserPhone_UNIQUE` (`UserPhone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `TblAddress`
--
ALTER TABLE `TblAddress`
  MODIFY `AddressId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TblCategory`
--
ALTER TABLE `TblCategory`
  MODIFY `CategoryId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TblCheck-in`
--
ALTER TABLE `TblCheck-in`
  MODIFY `ChickId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TblComentary`
--
ALTER TABLE `TblComentary`
  MODIFY `ComentId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TblCompany`
--
ALTER TABLE `TblCompany`
  MODIFY `CompanyId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TblEvent`
--
ALTER TABLE `TblEvent`
  MODIFY `EventId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TblFriend`
--
ALTER TABLE `TblFriend`
  MODIFY `FriendId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TblGroup`
--
ALTER TABLE `TblGroup`
  MODIFY `GroupId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TblImage`
--
ALTER TABLE `TblImage`
  MODIFY `ImageId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TblInterest`
--
ALTER TABLE `TblInterest`
  MODIFY `InterestId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `TblInterester`
--
ALTER TABLE `TblInterester`
  MODIFY `InterestId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `TblLike`
--
ALTER TABLE `TblLike`
  MODIFY `LikeId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TblMessage`
--
ALTER TABLE `TblMessage`
  MODIFY `MessageId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TblPeople`
--
ALTER TABLE `TblPeople`
  MODIFY `PeopleId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TblPost`
--
ALTER TABLE `TblPost`
  MODIFY `PostId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TblSession`
--
ALTER TABLE `TblSession`
  MODIFY `SessionId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TblSubCategory`
--
ALTER TABLE `TblSubCategory`
  MODIFY `SubCategoryId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TblUser`
--
ALTER TABLE `TblUser`
  MODIFY `UserId` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `TblCheck-in`
--
ALTER TABLE `TblCheck-in`
  ADD CONSTRAINT `fk_tbl_check-in_tbl_user1` FOREIGN KEY (`UserId`) REFERENCES `TblUser` (`UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `TblComentary`
--
ALTER TABLE `TblComentary`
  ADD CONSTRAINT `fk_TblComentario_TblPost1` FOREIGN KEY (`PostId`,`UserId`) REFERENCES `TblPost` (`PostId`, `UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `TblCompany`
--
ALTER TABLE `TblCompany`
  ADD CONSTRAINT `fk_TblCompany_TblCategory1` FOREIGN KEY (`CategoryId`) REFERENCES `TblCategory` (`CategoryId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `TblEvent`
--
ALTER TABLE `TblEvent`
  ADD CONSTRAINT `fk_tbl_eventos_tbl_user1` FOREIGN KEY (`UserId`) REFERENCES `TblUser` (`UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `TblFriend`
--
ALTER TABLE `TblFriend`
  ADD CONSTRAINT `fk_TblFriend_TblUser1` FOREIGN KEY (`UserId`) REFERENCES `TblUser` (`UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `TblImage`
--
ALTER TABLE `TblImage`
  ADD CONSTRAINT `fk_tbl_imagens_tbl_post1` FOREIGN KEY (`PostId`) REFERENCES `TblPost` (`PostId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `TblLike`
--
ALTER TABLE `TblLike`
  ADD CONSTRAINT `fk_TblGosto_TblPost1` FOREIGN KEY (`PostId`,`UserId`) REFERENCES `TblPost` (`PostId`, `UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `TblMessage`
--
ALTER TABLE `TblMessage`
  ADD CONSTRAINT `fk_TblMessage_TblFriend1` FOREIGN KEY (`FriendId`) REFERENCES `TblFriend` (`FriendId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_mensagess_tbl_user1` FOREIGN KEY (`UserId`) REFERENCES `TblUser` (`UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `TblMycompany`
--
ALTER TABLE `TblMycompany`
  ADD CONSTRAINT `fk_TblUser_has_TblCompany_TblCompany1` FOREIGN KEY (`CompanyId`) REFERENCES `TblCompany` (`CompanyId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TblUser_has_TblCompany_TblUser1` FOREIGN KEY (`UserId`) REFERENCES `TblUser` (`UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `TblMyinterest`
--
ALTER TABLE `TblMyinterest`
  ADD CONSTRAINT `fk_TblUser_has_TblInterest_TblInterest1` FOREIGN KEY (`InterestId`) REFERENCES `TblInterest` (`InterestId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TblUser_has_TblInterest_TblUser1` FOREIGN KEY (`UserId`) REFERENCES `TblUser` (`UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `TblPeople`
--
ALTER TABLE `TblPeople`
  ADD CONSTRAINT `fk_tbl_pessoa_tbl_user1` FOREIGN KEY (`UserId`) REFERENCES `TblUser` (`UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `TblPeoplegroup`
--
ALTER TABLE `TblPeoplegroup`
  ADD CONSTRAINT `fk_TblPeople_has_TblGroup_TblGroup1` FOREIGN KEY (`GroupId`) REFERENCES `TblGroup` (`GroupId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TblPeople_has_TblGroup_TblPeople1` FOREIGN KEY (`PeopleId`,`UserId`) REFERENCES `TblPeople` (`PeopleId`, `UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `TblPeople_has_TblCompany`
--
ALTER TABLE `TblPeople_has_TblCompany`
  ADD CONSTRAINT `fk_TblPeople_has_TblCompany_TblCompany1` FOREIGN KEY (`TblCompany_CompanyId`) REFERENCES `TblCompany` (`CompanyId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TblPeople_has_TblCompany_TblPeople1` FOREIGN KEY (`TblPeople_PeopleId`,`TblPeople_UserId`) REFERENCES `TblPeople` (`PeopleId`, `UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `TblPost`
--
ALTER TABLE `TblPost`
  ADD CONSTRAINT `fk_tbl_post_tbl_user1` FOREIGN KEY (`UserId`) REFERENCES `TblUser` (`UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `TblSession`
--
ALTER TABLE `TblSession`
  ADD CONSTRAINT `fk_tbl_sessao_tbl_user1` FOREIGN KEY (`UserId`) REFERENCES `TblUser` (`UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `TblSubCategory`
--
ALTER TABLE `TblSubCategory`
  ADD CONSTRAINT `fk_TblSubCategory_TblCategory1` FOREIGN KEY (`CategoryId`) REFERENCES `TblCategory` (`CategoryId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
